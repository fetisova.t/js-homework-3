/**
 * Created on 11.06.2019.
 */
//Описать своими словами для чего вообще нужны функции в программировании.
//Функции в программировании нужны для того, чтобы 1 набор действий можно было дублировать в разных участках кода
//Описать своими словами, зачем в функцию передавать аргумент.
//Передавать в функцию аргументы необходимо (если это предусмтрено вообще) так как заданный набор действий может требовать определенные данные для выполнения тела функциию, которые соответственно нужно передатьв  функцию для корректной отработки всех действий функции

/*Считать с помощью модального окна браузера два числа.
    Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено +, -, *, /.
Создать функцию, в которую передать два значения и операцию.
    Вывести в консоль результат выполнения функции.


    Не обязательное задание продвинутой сложности:

    После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал не числа, - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).*/

let firstNum = parseFloat(prompt('Enter first number: ')),
    secondNum = parseFloat(prompt('Enter second number: '));
while(isNaN(firstNum)|| isNaN(secondNum)){
    firstNum = parseFloat(prompt('You made a mistake, enter first number: ', firstNum));
    secondNum = parseFloat(prompt('You made a mistake, enter second number: ', secondNum));
}
let operationSign = prompt('Enter operation +, -, * or /', '+');
while(operationSign !=='+'&& operationSign !=='-'&& operationSign !=='*'&& operationSign !=='/' ){
    operationSign = prompt('Enter correct operation sign', '+');
}

function calc(num1, num2, operation) {
    switch(operation){
        case '+':
            return num1+num2;
        case '-':
            return num1-num2;
        case '*':
            return num1*num2;
        case '/':
            return num1/num2;
         default:
            return false;
    }
}

console.log(calc(firstNum, secondNum, operationSign));
